import React from 'react'
import {Link} from 'react-router-dom'
import {FaQuestion} from 'react-icons/fa'

function AboutIconLink() {
  return (
    <div className='about-lnk'>
      <Link to={{
        pathname:'/about',
        search:'?sort=name',//to można różne params umieszczać
      }}>
      <FaQuestion size={38}/>
      </Link>
      
    </div>
  )
}

export default AboutIconLink