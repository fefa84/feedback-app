
import {BrowserRouter as Router, Route, Routes,NavLink } from 'react-router-dom'
import {v4 as uuidv4} from 'uuid';//odpowiada za przypisanie id
import { useState } from "react";
import Header from "./components/Header";
import Card from './components/shared/Card';
import FeedbackList from "./components/FeedbackList";
import FeedbackData from "./data/FeedbackData";
import FeedbackStats from "./components/FeedbackStats";
import FeedbackForm from "./components/FeedbackForm";
import AboutPage from "./pages/AboutPage";
import React from 'react';
import AboutIconLink from './components/AboutiIonLink';
import Post from './components/Post';

function App() {

  const [feedback, setFeedback]=useState(FeedbackData);
  
  const addFeedback = (newFeedback)=>{
    newFeedback.id = uuidv4()
    setFeedback([newFeedback,...feedback])
    
  }
  
  const deleteFeedback = (id) => {
    if (window.confirm("Are you sure you want to delete?")) {
      setFeedback(feedback.filter((item) => item.id !== id))
    }
    
  }

  return (
  <Router>
    <Header/>
    <div className="container">
      <Routes>
        <Route 
// @ts-ignore
        exact path='/' element={
          <> 
          <FeedbackForm handleAdd={addFeedback}/>
          <FeedbackStats feedback={feedback}/>
          <FeedbackList feedback={feedback} handleDelete={deleteFeedback}/>
          </>
        }
        >
        </Route>
        <Route path='/about' element={<AboutPage/>}/>
        <Route path='/post/*' element={<Post/>}/>
        <Route path='/post/:id/:name' element={<Post/>}/>
      </Routes>
<Card>
  <NavLink to='/' activeclassName='active'>
    Home
  </NavLink>
  <NavLink to='/about' activeclassname='active'>
    About
  </NavLink>

</Card>
      <AboutIconLink/>
    </div>
       
  </Router>
  
  )
}

export default App;